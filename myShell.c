/*
 *  Author: Mikiya Habu <habutaso@gmail.com>
 *
 *  a simple shell
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>

#include <sys/types.h>
#include <sys/wait.h>

#define BUFSIZE  1024
#define ARGVSIZE 100

#define EXEC         1
#define REDIRECT     2
#define REDIRECT_ADD 21
#define REDIRECT_ERR 22
#define PIPE         3
#define CHDIR        4

const char whitespace[] = " \t\r\n\v";


int parsecmd(char **argv1, char **argv2, char *buf, char *ebuf)
{
	char *s;
	int  mode = EXEC;
	int  i = 0; int j = 0; int k = 0;

	s = buf;

	while (s < ebuf) {
		while (s < ebuf && strchr(whitespace, *s)) s++;
		if (ebuf <= s) return -1;

		argv1[i++] = s;
		while (s < ebuf && !strchr(whitespace, *s)) s++;
		*s = '\0'; 
		s++;
	}
	
	if (strcmp(argv1[0], "cd") == 0) {
		return CHDIR;
	}

	for (j = 0; j < i; j++) {
		if (strcmp(argv1[j], ">") == 0) {
			mode = REDIRECT;
			argv1[j] = NULL;
			continue;
		} else if (strcmp(argv1[j], ">>") == 0) {
			mode = REDIRECT_ADD;
			argv1[j] = NULL;
			continue;
		} else if (strcmp(argv1[j], "2>") == 0) {
			mode = REDIRECT_ERR;
			argv1[j] = NULL;
			continue;
		}else if (strcmp(argv1[j], "|") == 0) {
			mode = PIPE;
			argv1[j] = NULL;
			continue;
		}
		
		if (mode != EXEC) {
			argv2[k++] = argv1[j];
			argv1[j] = NULL;
		}
	}

	return mode;
}

void runredir(char **argv1, char **argv2, int mode)
{
	/*
	 * fd open()のファイルディスクリプタ
	 * stdout_fd 出力コピー先ファイルディスクリプタ
	 * base_fd コピー元ファイルディスクリプタ(1: 標準, 2: エラー)
	 * */
	int fd, stdout_fd, base_fd;

	if (mode == REDIRECT_ADD) {
		if ((fd = open(argv2[0], 
						O_WRONLY | O_APPEND, S_IRUSR | S_IWUSR  )) < 0) {
			perror("open");
			exit(-1);
		}
	} else {
		if((fd = open(argv2[0], 
						O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR)) < 0) {
			perror("open");
			exit(-1);
		}
	}

	if (mode == REDIRECT_ERR) {
		base_fd = 2;
	} else {
		base_fd = 1;
	}

	// 標準出力を新しいファイルディスクリプタに複製
	stdout_fd = dup(base_fd);
	close(base_fd);

	// fdをコピー元に複製
	dup2(fd, base_fd);

	if (execvp(argv1[0], argv1) < 0) {
	perror("execvp");
	}

	// 出力を元あったファイルディスクリプタに戻す。
	dup2(stdout_fd, base_fd);
	close(stdout_fd);

	close(fd);
}

void runpipe(char **argv1, char **argv2)
{
	/*
	 * fd 標準入出力用ファイルディスクリプタ
	 * filedes[2] パイプ
	 * */
	int fd;
	int filedes[2];
	pid_t l_pid, r_pid;

	if (pipe(filedes) < 0) {
		perror("pipe");
	}

	if ((l_pid = fork()) == 0) {
		// 左側のコマンドを実行
		fd = dup(1);
		close(1);
		dup2(filedes[1], 1);
		if (execvp(argv1[0], argv1) < 0) {
			perror("execvp");
		}
		dup2(fd, 1);
		close(fd);
	}

	if ((r_pid = fork()) == 0) {
		// 右側のコマンドを実行
		fd = dup(0);
		close(0);
		dup2(filedes[0], 0);
		if (execvp(argv2[0], argv2) < 0) {
			perror("execvp");
		}
		dup2(fd, 0);
		close(fd);
		
	}
	
	close(filedes[0]);
	close(filedes[1]);

	waitpid(l_pid, NULL, WNOHANG | WUNTRACED);
	waitpid(r_pid, NULL, WNOHANG | WUNTRACED);
}

int runcd(char **argv)
{
	if (argv[1] == NULL) argv[1] = "~";
	if (chdir(argv[1]) != 0) {
		perror("chdir");
	}
	return 1;
}

void runcmd(char *buf)
{
	char *argv1[ARGVSIZE];
	char *argv2[ARGVSIZE];

	memset(argv1, 0, ARGVSIZE);
	memset(argv2, 0, ARGVSIZE);
	
	switch (parsecmd(argv1, argv2, buf, &buf[strlen(buf)])) {
		case EXEC:
			if (fork() == 0) {
				if (execvp(argv1[0], argv1) < 0)
					perror("execvp");
			}
			wait(NULL);
			break;

		case REDIRECT:
			if (fork() == 0)
				runredir(argv1, argv2, REDIRECT);
			wait(NULL);
			break;

		case REDIRECT_ADD:
			if (fork() == 0)
				runredir(argv1, argv2, REDIRECT_ADD);
			wait(NULL);
			break;

		case REDIRECT_ERR:
			if (fork() == 0)
				runredir(argv1, argv2, REDIRECT_ERR);
			wait(NULL);
			break;

		case PIPE:
			if (fork() == 0)
				runpipe(argv1, argv2);
			wait(NULL);
			break;

		case CHDIR:
			runcd(argv1);
			break;

		default:
			break;
	}

}


int getcmd(char *buf, int len)
{
	char path[FILENAME_MAX];
	getcwd(path, FILENAME_MAX);

	printf("%s ", path);
	printf("> ");
	fflush(stdin);
	memset(buf, 0, len);
	fgets(buf, len, stdin);

	if (buf[0] == 0) return -1;
	return 0;
}

int main(int argc, char**argv)
{
	int status;
	static char buf[BUFSIZE];

	do {
		status = getcmd(buf, BUFSIZE);
		runcmd(buf);
	} while (status >= 0);

	exit(0);
}
